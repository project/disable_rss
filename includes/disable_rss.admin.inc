<?php

/**
 * @file
 * Contains page callback related functions.
 */

/**
 * Provides the form for the disable_rss setting.
 */
function disable_rss_admin_page($form, &$form_state) {
  form_load_include($form_state, 'inc', 'disable_rss', 'includes/disable_rss.admin');

  $form['disable_rss_is_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the function to disable the <code>/rss</code> page.'),
    '#default_value' => variable_get('disable_rss_is_enabled', TRUE),
  );

  $form['#submit'][] = 'disable_rss_admin_page_submit';

  return system_settings_form($form);
}

/**
 * Provides the form submit callback for disable_rss_admin_page().
 */
function disable_rss_admin_page_submit(&$form, &$form_state) {

  // Rebuild the menu as the variable disable_rss_is_enabled affects the menu.
  variable_set('menu_rebuild_needed', TRUE);
}
