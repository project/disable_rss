<?php

/**
 * @file
 * Page callback related functions.
 */

/**
 * Page callback which returns MENU_NOT_FOUND for the default /rss.xml.
 */
function disable_rss_page() {
  return MENU_NOT_FOUND;
}
